################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ReachAValueData.cpp \
../src/SinglyLinkedList.cpp \
../src/SinglyLinkedListFIFO.cpp \
../src/daysOfTheWeek.cpp \
../src/daysOfTheWeek_test.cpp \
../src/linkedListPractice.cpp \
../src/sortableObject.cpp \
../src/sortableObject_test.cpp 

OBJS += \
./src/ReachAValueData.o \
./src/SinglyLinkedList.o \
./src/SinglyLinkedListFIFO.o \
./src/daysOfTheWeek.o \
./src/daysOfTheWeek_test.o \
./src/linkedListPractice.o \
./src/sortableObject.o \
./src/sortableObject_test.o 

CPP_DEPS += \
./src/ReachAValueData.d \
./src/SinglyLinkedList.d \
./src/SinglyLinkedListFIFO.d \
./src/daysOfTheWeek.d \
./src/daysOfTheWeek_test.d \
./src/linkedListPractice.d \
./src/sortableObject.d \
./src/sortableObject_test.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/linkedListPractice.o: ../src/linkedListPractice.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"src/linkedListPractice.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


