inserting 5 = A
inserting 4 = R
inserting 3 = O
inserting 2 = D
inserting 1 = E
inserting 0 = F
After inserting, list contents are:
At 0x253d490 string = "A" next = 0x253d4b0
At 0x253d4b0 string = "R" next = 0x253d4d0
At 0x253d4d0 string = "O" next = 0x253d4f0
At 0x253d4f0 string = "D" next = 0x253d510
At 0x253d510 string = "E" next = 0x253d530
At 0x253d530 string = "F" next = 0
After sort, list contents are:
At 0x253d490 string = "A" next = 0x253d4f0
At 0x253d4f0 string = "D" next = 0x253d510
At 0x253d510 string = "E" next = 0x253d530
At 0x253d530 string = "F" next = 0x253d4d0
At 0x253d4d0 string = "O" next = 0x253d4b0
At 0x253d4b0 string = "R" next = 0

Removing one element at a time:
R
O
F
E
D
A
