################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/SinglyLinkedList.cpp \
../src/SinglyLinkedListFIFO.cpp \
../src/daysOfTheWeek.cpp \
../src/daysOfTheWeek_test.cpp \
../src/linkedFIFO.cpp \
../src/linkedListPractice.cpp \
../src/list.cpp \
../src/sortableObject.cpp \
../src/sortableObject_test.cpp 

OBJS += \
./src/SinglyLinkedList.o \
./src/SinglyLinkedListFIFO.o \
./src/daysOfTheWeek.o \
./src/daysOfTheWeek_test.o \
./src/linkedFIFO.o \
./src/linkedListPractice.o \
./src/list.o \
./src/sortableObject.o \
./src/sortableObject_test.o 

CPP_DEPS += \
./src/SinglyLinkedList.d \
./src/SinglyLinkedListFIFO.d \
./src/daysOfTheWeek.d \
./src/daysOfTheWeek_test.d \
./src/linkedFIFO.d \
./src/linkedListPractice.d \
./src/list.d \
./src/sortableObject.d \
./src/sortableObject_test.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -std=c++1y -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


