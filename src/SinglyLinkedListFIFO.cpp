/*
 * SinglyLinkedListFIFO.cpp
 *
 *  Created on: Nov 17, 2015
 *      Author: owner
 */

#include "SinglyLinkedListFIFO.h"

namespace std {

template <class T>
SinglyLinkedListFIFO<T>::SinglyLinkedListFIFO() {
	pObject = nullptr;
	pObject	= new (std::nothrow) SinglyLinkedList<T>;
}

template <class T>
SinglyLinkedListFIFO<T>::~SinglyLinkedListFIFO() {
	if (pObject != nullptr)
	{
		delete pObject;
		pObject	 = nullptr;
	}
	// TODO Auto-generated destructor stub
}

} /* namespace std */
