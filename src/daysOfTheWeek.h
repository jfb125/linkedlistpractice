/*
 * daysOfTheWeek.h
 *
 *  Created on: Oct 17, 2015
 *      Author: owner
 */

#ifndef DAYSOFTHEWEEK_H_
#define DAYSOFTHEWEEK_H_

namespace std {

class daysOfTheWeek {
public:
	enum theDays	{invalid = 0, monday, tuesday, wednesday, thursday, friday, saturday, sunday, invalidDay};

	int		compareTo(const daysOfTheWeek&);
	void	print();
	void	print(int d);

	string	toString();

	daysOfTheWeek&	operator =(const daysOfTheWeek&);
	daysOfTheWeek&	operator =(const int);
	bool	operator !=(const daysOfTheWeek&);
	bool	operator ==(const daysOfTheWeek&);
	bool	operator >(const daysOfTheWeek&);
	bool	operator <(const daysOfTheWeek&);
	bool	operator >=(const daysOfTheWeek&);
	bool	operator <=(const daysOfTheWeek&);


	daysOfTheWeek&	operator++();		// pre
	daysOfTheWeek&	operator--();		// pre

	daysOfTheWeek	operator++(int);	// post
	daysOfTheWeek	operator--(int);	// post

	int		get()		{	return theDay;	}
	void	set(int d)	{	theDay = d;		}

	daysOfTheWeek();
	daysOfTheWeek(const daysOfTheWeek &);
	daysOfTheWeek(const int d);

	virtual ~daysOfTheWeek();

private:
	int		theDay;
};

} /* namespace std */

#endif /* DAYSOFTHEWEEK_H_ */
