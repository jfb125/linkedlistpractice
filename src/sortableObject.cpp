/*
 * sortableObject.cpp
 *
 *  Created on: Dec 3, 2015
 *      Author: owner
 */

#include <iostream>

#include "sortableObject.h"

namespace std {

char 	sortableObject::toString()
{
	return initialized ? x : ' ';
}


void	sortableObject::toScreen()
{
	if (initialized)
		cout << x << endl;
	else
		cout << ' ' << endl;
}


/* ******************** */
/*		operator =		*/
/* ******************** */
sortableObject&	sortableObject::operator =(const sortableObject &rhs)
{
	// cout << "sortableObject::operator =(&)" << endl;
	initialized	= rhs.initialized;
	x			= rhs.x;
	return		*this;
}
sortableObject&	sortableObject::operator =(const int i)
{
	// cout << "sortableObject::operator =(int)" << endl;
	initialized	= true;
	x			= (char) i;
	return		*this;
}
sortableObject&	sortableObject::operator =(const char c)
{
	// cout << "sortableObject::operator =(char)" << endl;
	initialized	= true;
	x			= c;
	return		*this;
}


/* ******************** */
/*		operator ==		*/
/* ******************** */
bool			sortableObject::operator ==(const sortableObject &rhs)
{
	if (!initialized || !rhs.initialized)	return	false;
	if (x == rhs.x)							return  true;
	return false;
}
bool			sortableObject::operator ==(const int i)
{
	if (!initialized)	return	false;
	if (x == i)			return  true;
	return false;
}
bool			sortableObject::operator ==(const char c)
{
	if (!initialized)	return	false;
	if (x == c)			return  true;
	return false;
}

/* ******************** */
/*		operator !=		*/
/* ******************** */
bool			sortableObject::operator !=(const sortableObject &rhs){
	if (!initialized || !rhs.initialized)	return	false;
	if (x != rhs.x)							return	true;
	return	false;
}
bool			sortableObject::operator !=(const int i){
	if (!initialized)	return	false;
	if (x != (char) i)	return	true;
	return	false;
}
bool			sortableObject::operator !=(const char c){
	if (!initialized)	return	false;
	if (x != c)			return	true;
	return	false;
}


/* ******************** */
/*		operator >		*/
/* ******************** */
bool	sortableObject::operator >(const sortableObject &rhs)
{
	if (!initialized || !rhs.initialized)	return	false;
	if (x > rhs.x)							return	true;
	return false;
}
bool	sortableObject::operator >(const int i)
{
	if (!initialized)	return	false;
	if (x > i)			return	true;
	return false;
}
bool	sortableObject::operator >(const char c)
{
	if (!initialized)	return	false;
	if (x > c)			return	true;
	return false;
}


/* ******************** */
/*		operator >		*/
/* ******************** */
bool	sortableObject::operator >=(const sortableObject &rhs)
{
	if (!initialized || !rhs.initialized)	return	false;
	if (x >= rhs.x)							return	true;
	return false;
}
bool	sortableObject::operator >=(const int i)
{
	if (!initialized)	return	false;
	if (x >= i)			return	true;
	return false;
}
bool	sortableObject::operator >=(const char c)
{
	if (!initialized)	return	false;
	if (x >= c)			return	true;
	return false;
}


/* ******************** */
/*		operator <		*/
/* ******************** */
bool	sortableObject::operator <(const sortableObject &rhs)
{
	if (!initialized || !rhs.initialized)	return	false;
	if (x < rhs.x)							return	true;
	return false;
}
bool	sortableObject::operator <(const int i)
{
	if (!initialized)	return	false;
	if (x < i)			return	true;
	return false;
}
bool	sortableObject::operator <(const char c)
{
	if (!initialized)	return	false;
	if (x < c)			return	true;
	return false;
}


/* ******************** */
/*		operator <=		*/
/* ******************** */
bool	sortableObject::operator <=(const sortableObject &rhs)
{
	if (!initialized || !rhs.initialized)	return	false;
	if (x <= rhs.x)							return	true;
	return false;
}
bool	sortableObject::operator <=(const int i)
{
	if (!initialized)	return	false;
	if (x <= i)			return	true;
	return false;
}
bool	sortableObject::operator <=(const char c)
{
	if (!initialized)	return	false;
	if (x <= c)			return	true;
	return false;
}


/* ******************** */
/*		c-tors			*/
/* ******************** */
sortableObject::sortableObject() {
	// cout << "sortableObject::sortableObject()" << endl;

	initialized	= false;
	x			= '\xff';
}
sortableObject::sortableObject(int xin) {
	// cout << "sortableObject::sortableObject(int)" << endl;

	initialized	= true;
	x			= (char) xin;
}
sortableObject::sortableObject(char cin) {
	// cout << "sortableObject::sortableObject(char)" << endl;
	initialized	= true;
	x			= cin;
}
sortableObject::sortableObject(sortableObject &rhs) {
	// cout << "sortableObject::sortableObject(sortableObject &)" << endl;

	initialized	= rhs.initialized;
	x			= initialized ? rhs.x : '\xff';
}

sortableObject::~sortableObject() {
	// TODO Auto-generated destructor stub
	initialized	= false;
	x			= '\xff';
}

} /* namespace std */
