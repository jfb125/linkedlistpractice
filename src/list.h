/*
 * list.h
 *
 *  Created on: Apr 19, 2016
 *      Author: owner
 */

#ifndef LIST_H_
#define LIST_H_

#include <string>

namespace std {
#define	LIST_SIZE	10
class list {
public:
	list(int a = -1, int b = -1, int c = -1);
	virtual ~list();
	void 	print();
private :
	int	theList[LIST_SIZE];
};

} /* namespace std */

#endif /* LIST_H_ */
