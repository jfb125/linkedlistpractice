/*
 * sortableObject_test.cpp
 *
 *  Created on: Dec 3, 2015
 *      Author: owner
 */

#include <iostream>

#include "sortableObject.h"

#define	message(x,s)	cout << s << x.toString() << endl;
namespace std {

int		main (int argc, char *argv[])
{
	sortableObject	it;

	message(it, "Before assignment");
	it	= 'A';
	message(it, "After assignment");

	return 0;
}

} /* namespace std */
