#include <iostream>
#include <iomanip>
#include <fstream>
#include "SinglyLinkedList.h"

using namespace std;

//  This exchanges the object at [a] with the object at [b]
//  If index is a value greater than the number of elements-1,
//      no action is taken
//	If there is only one element,
//		no action is taken
//	If b is not supplied, a value of [a+1] is assumed
//
//	There are 3 specific algorithms:
//		a is 0 (head)
//			BpointedTo				= pointsToB->next
//			oldHead					= Head
//			head					= B
//			B->next					= oldHead->next
//			pointsToB->next 		= oldHead
//			oldHead->next			= BpointedTo

//		a & b are not adjacent (b > a+1)

template <class T>
void    SinglyLinkedList<T>::exchange(int a, int b)
{
	int	earlyIndex;
	int	laterCount;

#ifdef  DEBUG_EXCHANGE
	cout << "Entered 'SinglyLinedList<T>::exchange(" << a << ", "<< b << ")'" << endl;
#endif

	// if the list has 0 or only 1 element, return
	if (head == nullptr || head->next == nullptr)
		return;
	//	if the indices are bad
	if (a < 0)	return;
	if (a == b)	return;

	//	if 'b' is the default (-1)
	if (b < 0)
	{
		earlyIndex	= a;
		laterCount	= 1;
	}
	else
	if (a < b)
	{
		earlyIndex	= a;
		laterCount	= b-a;
	}
	else
	{
		earlyIndex	= b;
		laterCount	= a-b;
	}

#ifdef  DEBUG_EXCHANGE
	cout << "earlyIndex is " << earlyIndex << " and laterCount is " << laterCount << endl;
#endif

	if (isLoop())
		return;

	//	if A is the first element, there will be not 'pointsToA'

	if ( earlyIndex == 0)
	{
		node *previousHead	= head;
		node *pointsToB		= head;

		 while (--laterCount > 0) {
			pointsToB	= pointsToB->next;
			if (pointsToB == nullptr)
				return;
		}

#ifdef  DEBUG_EXCHANGE
		cout << "Before exchange: " << endl;
		cout << "  head      : " 	<< previousHead;
		cout << " -> " 				<< previousHead->next << endl;
		cout << "  pointsToB : " 	<< pointsToB;
		cout << " -> " 				<< pointsToB->next;
		cout << " -> " 				<< pointsToB->next->next << endl;
		cout << endl;
#endif
		node *BpointedTo	= pointsToB->next->next;

		//	If they are not contiguous
		if (pointsToB != head)
		{
			head					= pointsToB->next;		// A = B
			head->next				= previousHead->next;	// A->next =
			pointsToB->next			= previousHead;			// ... ->A
			pointsToB->next->next	= BpointedTo;			//
		}
		else
		{
			head					= head->next;	// B
			head->next				= previousHead;	// B->A
			head->next->next		= BpointedTo;	// B->A->C
		}

#ifdef  DEBUG_EXCHANGE
		cout << "After exchange: " << endl;
		cout << "  head      : " << head;
		cout << " -> " << head->next << endl;
		cout << "  pointsToB : " << pointsToB;
		cout << " -> " << pointsToB->next;
		cout << " -> " << pointsToB->next->next << endl;
		cout << endl;
#endif
	}
	else	// A is not head
	{
		node    *pointsToA  = head;


		while (--earlyIndex > 0)
		{
	#ifdef DEBUG_EXCHANGE
			cout << "finding early node: " << earlyIndex << " : " << pointsToA << ", " << pointsToA->next << endl;
	#endif
			pointsToA   = pointsToA->next;
			if (pointsToA == nullptr)
				return;
		}

#ifdef	DEBUG_EXCHANGE
		cout << "  pointsToA : " << pointsToA << ", " << pointsToA->next << ", " << pointsToA->next->next << endl;
#endif

		//	if there is no 'A', leave
		if (pointsToA->next == nullptr)
			return;

		node	*pointsToB	= pointsToA;

		do
		{
	#ifdef DEBUG_EXCHANGE
			cout << "finding later node: " << laterCount << " : " << pointsToB << ", " << pointsToB->next << endl;
	#endif
			pointsToB	= pointsToB->next;
			if (pointsToB == nullptr)
				return;
		}
		while (--laterCount > 0);

#ifdef  DEBUG_EXCHANGE
		cout << "  pointsToB : " << pointsToB << ", " << pointsToB->next << ", " << pointsToB->next->next << endl;
#endif

		//	if there is no 'B', leave
		if (pointsToB->next == nullptr)
			return;

		//  pointsToA.next => A.next => B.next => pointsFromB
		node *A             = pointsToA->next;
		node *APointsTo		= pointsToA->next->next;
		node *B             = pointsToB->next;
		node *BPointsTo		= pointsToB->next->next;

	#ifdef  DEBUG_EXCHANGE
		cout << "Before exchange: " << endl;
		cout << "  pointsToA : " << pointsToA;
		cout << " -> " << pointsToA->next;
		cout << " -> " << pointsToA->next->next << endl;
		cout << "  pointsToB : " << pointsToB;
		cout << " -> " << pointsToB->next;
		cout << " -> " << pointsToB->next->next << endl;
		cout << endl;
	#endif
		if (A->next == B)					// consecutive
		{									// 	p2A->A	A->B	A->B	B->Bp2
			pointsToA->next	= B;			// 	p2A->B	A->B	A->B	B->Bp2
			B->next			= A;			// 	p2A->B	B->A	A->B	B->A
			A->next			= BPointsTo;	// 	p2A->B	B->A	A->Bp2	B->A
		}
		else								// not consecutive
		{									// 	p2A->A	A->Ap2	p2B->B	B->Bp2
			pointsToA->next	= B;			//	p2A->B	A->Ap2	p2B->B	B->Bp2
			B->next			= APointsTo;	//	p2A->B  B->Ap2  p2B->B  B->Ap2
			pointsToB->next	= A;			//  p2A->B  B->Ap2  p2B->A  B->Bp2
			A->next			= BPointsTo;	//	p2A->B	B->Ap2	p2B->A	A->Bp2
		}

	#ifdef  DEBUG_EXCHANGE
		cout << "After exchange: " << endl;
		cout << "  pointsToA : " << pointsToA;
		cout << " -> " << pointsToA->next;
		cout << " -> " << pointsToA->next->next << endl;
		cout << "  pointsToB : " << pointsToB;
		cout << " -> " << pointsToB->next;
		cout << " -> " << pointsToB->next->next << endl;
		cout << endl;
	#endif
	}
}


/*
 * Inserts an element at the end of the list
 */
template<class T>
void    SinglyLinkedList<T>::insert(T s) throw(int)
{
	if (isLoop())           throw(1);

	if (head != nullptr)
	{
		node *p = head;

		//  Find the end of the list
		while (p->next != nullptr)
			p   = p->next;

		//  create a node at the end of the list
		p->next             = new struct node;
		p->next->object     = s;
		p->next->next       = nullptr;
	}
	else
	//if (head == nullptr)
	{
		head            = new struct    node;
		head->object    = s;
		head->next      = nullptr;
	}
	num++;
}

template<class T>
void	SinglyLinkedList<T>::makeArrayOfPointersToNodes(struct node **pd)
{
	struct node *ps	= head;

	while (ps != nullptr)
	{
		*pd++	= ps;
		ps		= ps->next;
	}
}


template<class T>
void	SinglyLinkedList<T>::insertionsort(struct node **base)
{
	struct node **dest;
	struct node *tmp;

#ifdef	_DEBUG_SINGLY_LINKED_LIST_INSERTION_SORT
	cout << "Prior to sort: " << endl;

	dest	= base;
	for (int i = 0; i != num; i++)
	{
		cout	<< setw(4) << setfill(' ') << i << ": "
		 		<< setw(8) << setfill('0') << *dest << " ("
		 		<< setw(10) << setfill(' ') << (*dest)->object.toString() << ") -> "
				<< setw(8) << setfill('0') << (*dest)->next << endl;
		dest++;
	}
#endif

	//	Insertion sort
	dest	= base;
	for (int i = 1; i != num; i++)
	{
		for (int j = i; j != 0 ; j--)
		{
			if (base[j-1]->object > base[j]->object)
			{
				tmp			= base[j];
				dest[j]		= base[j-1];
				base[j-1]	= tmp;
			}
		}
	}

#ifdef	_DEBUG_SINGLY_LINKED_LIST_INSERTION_SORT
	cout << "Pointers after sort:" << endl;
	dest	= base;
	for (int i = 0; i != num; i++)
	{
		cout	<< setw(4) << setfill(' ') << i << ": "
		 		<< setw(8) << setfill('0') << *dest << " ("
		 		<< setw(10) << setfill(' ') << (*dest)->object.toString() << ") -> "
				<< setw(8) << setfill('0') << (*dest)->next << endl;
		dest++;
	}
#endif

//	head	= *base;
//	dest	= base;
//	tmp		= head;
//	for (int i = 0 ; i != num ; i++)
//	{
//		tmp->next	= *dest++;
//		tmp			= tmp->next;
//		tmp->next	= nullptr;
//	}
}


template<class T>
bool    SinglyLinkedList<T>::isEmpty()   { return head == nullptr;   }

template<class T>
bool    SinglyLinkedList<T>::isLoop()
{
#ifdef	DEBUG_IS_LOOP
	cout << "isLoop()" << endl;
#endif
	bool	returnValue	= false;

	if (head != nullptr)
	{
		node *fast  = head;
		node *slow  = head;
		bool    advanceSlow = false;

		while (fast->next != nullptr)
		{
			fast    = fast->next;
#ifdef	DEBUG_IS_LOOP
			cout    << "fast:" << fast << ", slow:" << slow << endl;
#endif
			if (fast == slow)
			{
				returnValue	= true;
				break;
			}

			if (fast->next == nullptr)
				break;


			if (advanceSlow)
				slow    = slow->next;

			advanceSlow = !advanceSlow;
		}
	}

#ifdef	DEBUG_IS_LOOP
	if (returnValue == false)
		cout << "isLoop() is false" << endl;
	else
		cout << "isLoop() is true" << endl;
#endif

	return  returnValue;
}

#undef	_DEBUG_SINGLY_LINKED_LIST_SORT


/* ********************************************************	*/
/*															*/
/*	**base	- the first element in the list of ptrs to type	*/
/*	lo		- the index of the first element				*/
/*	hi		- the index of the last element					*/
/*															*/
/* ********************************************************	*/

template<class T>
int	SinglyLinkedList<T>::partition(struct node **base, int lo, int hi)
{
	struct  node *pivot	= base[lo];
	int	left	= lo;
	int right	= hi;
	struct 	node *tmp;

	auto xchng = [&] (int x, int y){
				tmp 	= base[x];
				base[x] = base[y];
				base[y] = tmp;
			};

	while(1)
	{
		//	find the first left side element > pivot
		while (base[++left]->object <= pivot->object)
			//	special case:  7654321 is handled here:
			if (left >= hi)	break;

		//	find the first right side element < pivot
		while (base[right]->object > pivot->object)	right--;

		// if pointers have crossed, leave
		if (right <= left)			break;

		//	exchange the two elements that are out-of-ordered
		xchng(left,right);
	}

	//	avoid special case, 1777777
	if (right != lo)	xchng(lo,right);
	return	right;
}


#if 0
template<class T>
void	SinglyLinkedList<T>::print(SinglyLinkedList<T>::node **base)
{
	cout << "Pointers before sort: " << endl;
	for (int i = 0; i != num; i++)
	{
		cout	<< setw(4) << setfill(' ') << i << ": "
		 		<< setw(8) << setfill('0') << *base << " ("
		 		<< setw(10) << setfill(' ') << (*base)->object.toString() << ") -> "
				<< setw(8) << setfill('0') << (*base)->next << endl;
		base++;
	}
}
#endif

template<class T>
void    SinglyLinkedList<T>::print(fstream &out)
{
	struct node *p  = head;
	while (p != nullptr)
	{
		cout    << "At " << p << " string = \"" << p->object.toString()
				<< "\" next = " <<  p->next << endl;
		p   = p->next;
	}

	if (out != nullptr)
	{
		p   = head;
		while (p != nullptr)
		{
			out     << "At " << p << " string = \"" << p->object.toString()
					<< "\" next = " <<  p->next << endl;
			p   = p->next;
		}
	}
}


template<class T>
void    SinglyLinkedList<T>::putALoopInTheList()
{
	struct node *p  = head;

	while(p->next != nullptr)
		p   = p->next;

	p->next = head;
	cout << "last->next == head" << endl;
}


template<class T>
void	SinglyLinkedList<T>::quicksort(struct node **base, int lo, int hi)
{
	if (lo >= hi)	return;
	int	pi	= partition(base,lo,hi);
	quicksort(base,lo,pi-1);
	quicksort(base,pi+1,hi);
};


/*
 *  remove's tail element
 */

template<class T>
T       SinglyLinkedList<T>::remove()    throw(int)
{
	if (isLoop())           throw(1);

	struct node *p = head;


	//  list has 0 elements in it
	if (p == nullptr)   throw(1);

	//  list has 1 element in it
	if (p->next == nullptr)
	{
		T   returnValue = head->object;
		delete head;
		head            = nullptr;
		if (num)	num--;
		return  returnValue;
	}

	//  lest has at least 2 elements in it
	while (p->next->next != nullptr)
		p   = p->next;

	T   returnValue     = p->next->object;

	delete p->next;
	p->next = nullptr;

	if (num)	num--;
	return  returnValue;
}


template<class T>
void	SinglyLinkedList<T>::reverse()   throw(int)
{
	if (isLoop())   throw(1);

	if (head == nullptr || head->next == nullptr)
		return;

#ifdef  DEBUG_REVERSE
	cout    <<  "head is at "       << head
			<<  " and contains "    << head->object.toString()
			<<  " and points to "   << head->next
			<<  endl;
#endif

	struct node *previous	= nullptr;
	struct node *current	= head;
	struct node *next;

	while (next != nullptr)
	{
		cout 	<< "Before: " << setw(8) << setfill('0') << previous
				<< "-> " << setw(8) << setfill('0') << current
				<< "-> " << setw(8) << setfill('0') << current->next
				<< endl;
		//	store the current node's .next
		next			= current->next;
		//	overwrite with the previous node
		current->next	= previous;
		//	make a copy of this node
		previous		= current;
		//	move to the next node
		current			= next;
		cout 	<< "After:  " << setw(8) << setfill('0') << previous
				<< "-> " << setw(8) << setfill('0') << current;
		if (current != nullptr)
			cout<< "-> " << setw(8) << setfill('0') << current->next;
		cout	<< endl;
	}

	head	= previous;
	#ifdef  DEBUG_REVERSE
	cout    <<  "head is at "       << head
			<<  " and contains "    << head->object.toString()
			<<  " and points to "   << head->next
			<<  endl;
	#endif
}


template<class T>
void	SinglyLinkedList<T>::sort()
{
	struct node **p	= new struct node*[num];
	makeArrayOfPointersToNodes(p);

#ifdef	_SINGLY_LINKED_LIST_USE_INSERTION_SORT
	insertionsort(p);
#else
	quicksort(p,0,num-1);
#endif

	storeArrayOfPointersToNodes(p);
    delete[] p;
};


template<class T>
void	SinglyLinkedList<T>::storeArrayOfPointersToNodes(struct node **source)
{
	/*
	 * At entry, each element in source points to an unspecified next
	 *   [] 	*	 next				head
	 *	1000 : 2010('A')->2020('C')		5000: 2000('B')->2010('A')->2020('C')->nullptr
	 *	1004 : 2000('B')->2010('A')
	 *	1008 : 2020('C')->nullptr
	 *
	 * At exit, each element in source points to the next element
	 * 	 []		*	 next				head
	 *	1000 : 2010('A')->2000('B')		5000: 2010('A')->2000('B')->2020('C')->nullptr
	 *	1004 : 2000('B')->2020('C')
	 *	1008 : 2020('C')->nullptr
	 */

	head	= *source;

	for (int i = num-1; i; i--)
	{
		(*source)->next	= *(source+1);
		source++;
	}
	(*source)->next	= nullptr;
}

template<class T>
SinglyLinkedList<T>::SinglyLinkedList()
{
	head    = nullptr;
	num		= 0;
}


template<class T>
SinglyLinkedList<T>::~SinglyLinkedList()
{
	struct node *n;
	while (head != nullptr)
	{
		cout    << "myLinkedList destructor: head " << head << " has next of " << head->next << endl;
		n           = head->next;
		head->next  = nullptr;
		delete  head;
		head        = n;
	}
}
