/*
 * daysOfTheWeek_test.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: owner
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>

#include "daysOfTheWeek.h"
using namespace std;


int	Qmain()
{
	srand (time(NULL));

	daysOfTheWeek	day(daysOfTheWeek::monday);
	daysOfTheWeek	anotherDay = day;
	daysOfTheWeek	yetAnotherDay(anotherDay);

	daysOfTheWeek 	p[1024];

	for (int i = 0; i != 1024; i++)
		p[i]	= rand()%7+1;

	for (daysOfTheWeek &number : p)
		std::cout << number.toString() << endl;

	cout	<< setw(15) << ' ';
	for (int i = 1; i != 8; i++)
	{
		cout << setw(12);
		day.print(i);
	}
	cout << endl;

	for (int i = 1; i != 8; i++)
	{
		cout << setw(15);
		anotherDay = daysOfTheWeek::monday;
		day.print();
		for (int j = 1; j != 8; j++)
		{
			if (day == anotherDay)
				cout << setw(12) << "  == ";
			else
			if (day < anotherDay)
				cout << setw(12) << "  <  ";
			else
			if (day > anotherDay)
				cout << setw(12) << "  >  ";
			else
				cout << setw(12) << "error";
			anotherDay++;
		}
		cout << endl;
		day++;
	}

	return 0;
}
