/*
 * SinglyLinkedListFIFO.h
 *
 *  Created on: Nov 17, 2015
 *      Author: owner
 */

#ifndef SINGLYLINKEDLISTFIFO_H_
#define SINGLYLINKEDLISTFIFO_H_

#include "SinglyLinkedList.h"

namespace std {

template <class T>
class SinglyLinkedListFIFO: public SinglyLinkedList<T> {
public:
	SinglyLinkedListFIFO();
	virtual ~SinglyLinkedListFIFO();

private:
	SinglyLinkedList<T>	*pObject;
};

} /* namespace std */

#endif /* SINGLYLINKEDLISTFIFO_H_ */
