//============================================================================
// Name        : linkedListPractice.cpp
// Author      : Joe Baker
// Version     :
// Copyright   : Personal
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>


//#include "ReachAValueData.h"

using namespace std;

#include "sortableObject.h"
#include "SinglyLinkedList.h"
#include "SinglyLinkedList.cpp"
#include "linkedFIFO.h"
#include "list.h"

int main() {

	cout 	<< "Started linkedListPractice built on "
			<< __DATE__
			<< " at " << __TIME__ << endl;

	linkedFIFO	stack;

	for(int i = 0; i != 10; i++) {
		stack.push(i);
	}
	for(int i = 0; i != 10; i++) {
		stack.pop();
	}
	return	0;

	cout    << "Made change to push into github" << endl;
	fstream	input;
	fstream output;

	char	initializationValues[]	= "FEDORA";
	char	*src_ptr;

//	reachAValueList.insert(reachAValueSequence_1);
//	reachAValueList.insert(reachAValueSequence_2);

//	printAllReachAValueProfiles(reachAValueProfiles);
//	printReachAValueData(reachAValueList.remove());

	srand (time(NULL));

//	for (int i = 0; i != 1024; i++)
//	{
//		initializationValues[i] = rand()%7+1;
//	}

	output.open("linkedListPracticeOuput.txt", ios_base::out);
	input.open("linkedListPracticeInput.txt", ios_base::in);

	SinglyLinkedList<sortableObject>	list;

	if (input.bad())	return -1;

	src_ptr	= initializationValues;

	sortableObject x;

	for (int i = 5; i >= 0 ; i--)
//	while (!input.eof())
	{
//		getline(input, oneline);
//		cout 	<< "Read: " << oneline << endl;
//		output	<< "Read: " << oneline << endl;
//		if (oneline == "")
//		{
//			cout << "continue because empty line read" << endl;
//			continue;
//		}
		x = initializationValues[i];
		list.insert(x);
		cout	<< "inserting " << i << " = " << x.toString() << endl;
		output	<< "inserting " << i << " = " << x.toString() << endl;
		src_ptr++;
	}

	cout		<< "After inserting, list contents are:" << endl;
	output		<< "After inserting, list contents are:" << endl;
	list.print(output);

	if (list.isLoop())	cout << "List contains a loop." << endl;
	else				cout << "No loop in the list." << endl;

//	cout	<< "Putting a loop in the list" << endl;
//	list.putALoopInTheList();
//	if (list.isLoop())	cout << "List contains a loop." << endl;
//	else				cout << "No loop in the list." << endl;
//	for (int i = 0; i != 7; i++)
//	{
//		for (int j = i+1; j != 7; j++)
//		{
//			cout 	<< endl << endl << endl << "Exchange element " << i << " with " << j << endl;
//			output	<< endl << endl << endl << "Exchange element " << i << " with " << j << endl;
//			list.exchange(i, j);
//			list.print(output);
//			cout 	<< "Restoring list " << endl << endl;
//			output	<< "Restoring list " << endl << endl;
//			list.exchange(i, j);
//			list.print(output);
//		}
//	}
//	cout 	<< endl << "Removing one element at a time:" << endl;
//	output 	<< endl << "Removing one element at a time:" << endl;
//
//	while (!list.isEmpty())
//	{
////		list.reverse();
//		try
//		{
//			oneDay		= list.remove();
//		}
//		catch(...) {
//			cout 	<< "Remove exception happened." << endl;
//			output	<< "Remove exception happened." << endl;
//			input.close();
//			output.close();
//			return -1;
//		}
//		cout 	<< oneDay.toString() << endl;
//		output 	<< oneDay.toString() << endl;
//	}
//
//	list.reverse();
//	cout	<< "After 1st reversing, the lists contents are:" << endl;
//	list.print(output);

	list.sort();

	cout 	<< "After sort, list contents are:" << endl;
	output	<< "After sort, list contents are:" << endl;

	list.print(output);

	cout 	<< endl << "Removing one element at a time:" << endl;
	output 	<< endl << "Removing one element at a time:" << endl;

	while (!list.isEmpty())
	{
		try
		{
			x		= list.remove();
		}
		catch(...) {
			cout 	<< "Remove exception happened." << endl;
			output	<< "Remove exception happened." << endl;
			input.close();
			output.close();
			return -1;
		}
		cout 	<< x.toString() << endl;
		output 	<< x.toString() << endl;
	}

	input.close();
	output.close();
	return 0;
}
