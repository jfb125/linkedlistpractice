/*
 * linkedFIFO.cpp
 *
 *  Created on: Apr 19, 2016
 *      Author: owner
 */

#include "linkedFIFO.h"

namespace std {

int		linkedFIFO::pop(){
	struct node *oldTop	= top;	// The top will be removed
	int retVal			= 0;

	if(oldTop != nullptr) {		// If there is an element on the stack
		retVal	= oldTop->v;	// Set the value aside
		top		= oldTop->n;	// The top is now the old top's next
		oldTop->n	= nullptr;	// Pointer safety
		delete oldTop;			// Release the memory
	}
	oldTop	= nullptr;	// Pointer safety
	printState();
	return	retVal;
}

void	linkedFIFO::printState(){
	struct node *p	= top;
	cout << "linkedFIFO state:" << endl;
	while(p != nullptr) {
		cout << p << " (" << p->v << ") .. " << p->n << endl;
		p	= p->n;
	}
	cout << p << endl;
	p	= nullptr;	// Pointer safety
}

void	linkedFIFO::push(int x) {
	struct node *newTop	= new struct node;

	newTop->n	= top;		// This will point to the old top of the stack
	newTop->v	= x;		// Store the value
	newTop->p	= nullptr;	// Not used in a singly linked list
	top			= newTop;	// This is the new top of the stack
	newTop		= nullptr;	// Pointer safety
	printState();
}

linkedFIFO::linkedFIFO() {
	cout << "linkedFIFO ctor" << endl;
	top	= nullptr;
}

linkedFIFO::~linkedFIFO() {
	cout << "linkedFIFO dtor" << endl;
	struct node * oldTop = top;
	while(oldTop != nullptr) {
		printState();
		oldTop	= top;
		top		= top->n;
		delete	oldTop;
	}
	top	= nullptr;	// Pointer safety
}

} /* namespace std */
