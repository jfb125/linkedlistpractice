/*
 * list.cpp
 *
 *  Created on: Apr 19, 2016
 *      Author: owner
 */

#include "list.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>

namespace std {

void	list::print() {
	for(int i = 0; i != 10 ; i++) {
		cout << "[" << i << "]" << " " << setw(4) << theList[i] << endl;
	}
}


list::list(int a, int b, int c) {
	memset(theList, 0, sizeof(int) * LIST_SIZE);
	theList[0]	= a;
	theList[1]	= b;
	theList[2]	= c;
}

list::~list() {
	// TODO Auto-generated destructor stub
}

} /* namespace std */
