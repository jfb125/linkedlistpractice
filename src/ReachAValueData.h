/*
 * ReachAValueData.h
 *
 *  Created on: Oct 25, 2015
 *      Author: owner
 */

#ifndef REACHAVALUEDATA_H_
#define REACHAVALUEDATA_H_

void	applicationControlUpdateMotor();
void	bpvClosed();

typedef	struct	ReachAValueProfile
{
	double	flowRate;		// 0x04
	double	nextTarget;		// 0x08
	bool	valid;			// 0x0c
	bool	exit;			// 0x0e
	void	(*apf[5])();	// 0x10
}	ReachAValueProfile_t;

typedef	struct	ReachAValueProfileSummary
{
	const char							*description;
	const unsigned						byteSize;
	const struct	ReachAValueProfile	*values;
}	ReachAValueProfileSummary_t;

extern	const	ReachAValueProfileSummary_t reachAValueProfiles[];

void	printOneReachAValueProfile(const ReachAValueProfileSummary_t *profiles);
void	printAllReachAValueProfiles(const ReachAValueProfileSummary_t *profile);

#undef	REACH_A_VALUE_PROFILE
#undef  REACH_A_VALUE_TUPLET

#define REACH_A_VALUE_PROFILE_001               	\
	REACH_A_VALUE_PROFILE( \
	"Profile 001",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.120, 40.0, true,false,{bpvClosed, applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.100, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.080, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.071, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.062, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.053, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.044, 72.5, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_002                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 002",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.132, 40.0, true,false,{bpvClosed, applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.110, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.088, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.078, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.068, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.058, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.048, 72.5, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 75.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_003                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 003",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.108, 40.0, true,false,{bpvClosed,applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.090, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.072, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.064, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.058, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.048, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 72.5, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_004                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 004",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.144, 40.0, true,false,{bpvClosed, applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.120, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.096, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.085, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.074, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.064, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.053, 75.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.046, 77.5, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 80.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_005                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 005",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.096, 40.0, true,false,{bpvClosed, applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.080, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.064, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.057, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.050, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.042, 66.5, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_006                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 006",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.156, 40.0, true,false,{bpvClosed, applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.130, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.104, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.092, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.081, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.069, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.057, 75.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.048, 80.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 82.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})      \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_062                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 062",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.156, 40.0, true,false,{bpvClosed, applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.130, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.104, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.092, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.081, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.069, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.057, 80.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.048, 90.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 92.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_007                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 007",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.086, 40.0, true,false,{bpvClosed, applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.072, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.058, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.051, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.046, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 66.5, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_008                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 008",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.168, 40.0, true,false,{bpvClosed,applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.140, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.112, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.099, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.087, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.074, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.062, 75.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.051, 80.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 82.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_009                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 009",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.072, 40.0, true,false,{bpvClosed,applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.060, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.048, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.044, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_010                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 010",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.168, 40.0, true,false,{bpvClosed,applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.168, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.112, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.112, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.099, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.099, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.087, 75.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.087, 80.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.062, 90.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.051, 100.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.040, 102.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_102                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 102",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.168, 40.0, true,false,{bpvClosed,applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.168, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.112, 55.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.112, 60.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.099, 65.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.099, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.087, 75.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.087, 80.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.077, 90.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.062, 100.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_103                \
		REACH_A_VALUE_PROFILE( \
		"Profile 103",		\
{                                           \
    REACH_A_VALUE_TUPLET(0.168, 40.0, true,false,{bpvClosed,applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.168, 50.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#define REACH_A_VALUE_PROFILE_104                 \
		REACH_A_VALUE_PROFILE( \
		"Profile 104",						\
{                                           \
    REACH_A_VALUE_TUPLET(0.168, 50.0, true,false,{bpvClosed,applicationControlUpdateMotor,NULL,NULL,NULL}) \
    REACH_A_VALUE_TUPLET(0.120, 70.0, true,false,{applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
    REACH_A_VALUE_TUPLET(0.000, 00.0, true,true, {applicationControlUpdateMotor,NULL,NULL,NULL,NULL})       \
})

#endif /* REACHAVALUEDATA_H_ */
