/*
 * linkedFIFO.h
 *
 *  Created on: Apr 19, 2016
 *      Author: owner
 */

#ifndef LINKEDFIFO_H_
#define LINKEDFIFO_H_

#include <iostream>

namespace std {

struct node {
	int	v;
	struct node *n;
	struct node *p;
};

class linkedFIFO {
public:
	linkedFIFO();
	virtual ~linkedFIFO();
	void	push(int);
	int		pop();
private:
	struct node *top;
	void	printState();
};

} /* namespace std */

#endif /* LINKEDFIFO_H_ */
