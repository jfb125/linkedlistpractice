/*
 * sortableObject.h
 *
 *  Created on: Dec 3, 2015
 *      Author: owner
 */

#ifndef SORTABLEOBJECT_H_
#define SORTABLEOBJECT_H_

namespace std {

class sortableObject {
public:

	char 	toString();
	void	toScreen();

	sortableObject&	operator =(const sortableObject&);
	sortableObject&	operator =(const int);
	sortableObject&	operator =(const char);

	bool	operator !=(const sortableObject&);
	bool	operator !=(const int);
	bool	operator !=(const char);

	bool	operator ==(const sortableObject&);
	bool	operator ==(const int);
	bool	operator ==(const char);

	bool	operator >(const sortableObject&);
	bool	operator >(const int);
	bool	operator >(const char);

	bool	operator <(const sortableObject&);
	bool	operator <(const int);
	bool	operator <(const char);

	bool	operator >=(const sortableObject&);
	bool	operator >=(const int);
	bool	operator >=(const char);

	bool	operator <=(const sortableObject&);
	bool	operator <=(const int);
	bool	operator <=(const char);

//
//	daysOfTheWeek&	operator++();		// pre
//	daysOfTheWeek&	operator--();		// pre
//
//	daysOfTheWeek	operator++(int);	// post
//	daysOfTheWeek	operator--(int);	// post

	sortableObject();
	sortableObject(char c);
	sortableObject(int x);
	sortableObject(sortableObject &rhs);
	virtual ~sortableObject();

private:
	char	x;
	bool	initialized;
};

} /* namespace std */

#endif /* SORTABLEOBJECT_H_ */
