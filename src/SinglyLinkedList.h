/*
 * SinglyLinkedList.h
 *
 *  Created on: Nov 11, 2015
 *      Author: owner
 */

#ifndef SINGLYLINKEDLIST_H_
#define SINGLYLINKEDLIST_H_

//#define	DEBUG_EXCHANGE
//#define	DEBUG_REVERSE

#include <iostream>
#include <fstream>

template <class T>
class   SinglyLinkedList
{
public:
	void	exchange(int a, int b = -1);
	void	insert(T)	throw(int);
	bool	isEmpty();
	bool	isLoop();
	void    print(std::fstream &out);
    void    putALoopInTheList();
    T       remove()    throw(int);
    void    reverse()   throw(int);
    void	sort();

    SinglyLinkedList();
    ~SinglyLinkedList();
private:
    struct  node
    {
        T               object;
        struct  node    *next;
    };
    struct node *head;

    void		makeArrayOfPointersToNodes(struct node **destination);
    void		storeArrayOfPointersToNodes(struct node **source);
    void		insertionsort(struct node **source);
    void		quicksort(struct node **source, int n);
    void		quicksort(struct node **source, int lo, int hi);
    int			partition(struct node **source, int lo, int hi);
    int			num;
};




#endif /* SINGLYLINKEDLIST_H_ */
