/*
 * daysOfTheWeek.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: owner
 */

#include <iostream>
#include "daysOfTheWeek.h"

namespace std {

int		daysOfTheWeek::compareTo(const daysOfTheWeek& that)
{
	if (theDay == that.theDay)	return	0;
	if (theDay < that.theDay)	return -1;
	return	1;
}

daysOfTheWeek&	daysOfTheWeek::operator =(const daysOfTheWeek& rhs)
{
	theDay	= rhs.theDay;
	return *this;
}
daysOfTheWeek&	daysOfTheWeek::operator =(const int d)
{
	theDay	= d;
	return *this;
}
//	is equal to
bool	daysOfTheWeek::operator ==(const daysOfTheWeek &rhs){
	return rhs.theDay == theDay;
}
//	is not equal to
bool	daysOfTheWeek::operator !=(const daysOfTheWeek &rhs)
{
	return rhs.theDay != theDay;
}
//	is less than
bool	daysOfTheWeek::operator <(const daysOfTheWeek &rhs)
{
	int tmp = rhs.theDay - theDay;
	if (tmp == +1 	|| tmp == +2 	|| tmp == +3
	 || tmp == -4 	|| tmp == -5    || tmp == -6)
		return	true;
	return	false;
}
//	is greater than
bool	daysOfTheWeek::operator >(const daysOfTheWeek &rhs)
{
	int	tmp	= rhs.theDay-theDay;
	if ((tmp < 0 && tmp > -4) || (tmp > 3 && tmp < 7))
		return	true;
	return	false;
}
//	is greater than or equal
bool	daysOfTheWeek::operator >=(const daysOfTheWeek &rhs)
{
	return rhs.theDay > theDay || rhs.theDay == theDay;
}
//	is less than or equal
bool	daysOfTheWeek::operator <=(const daysOfTheWeek &rhs)
{
	return rhs.theDay < theDay || rhs.theDay == theDay;
}
//	pre increment
daysOfTheWeek&	daysOfTheWeek::operator++(){
	if (theDay == sunday)
		theDay	= monday;
	else
		theDay++;
	return	*this;
}
//	pre decrement
daysOfTheWeek&	daysOfTheWeek::operator--(){
	if (theDay == monday)
		theDay	= sunday;
	else
		theDay--;
	return	*this;
}
//	post increment
daysOfTheWeek	daysOfTheWeek::operator++(int){
	daysOfTheWeek result(theDay);
	++(*this);
	return result;
}
//	post decrement
daysOfTheWeek	daysOfTheWeek::operator--(int){
	daysOfTheWeek result(theDay);
	--(*this);
	return result;
}


void	daysOfTheWeek::print()
{
	print(theDay);
}


void	daysOfTheWeek::print(int d)
{
	switch (d)	{
	case	monday	:
		cout	<< "monday";
		break;
	case	tuesday	:
		cout	<< "tuesday";
		break;
	case	wednesday	:
		cout 	<< "wednesday";
		break;
	case	thursday	:
		cout	<< "thursday";
		break;
	case	friday		:
		cout	<< "friday";
		break;
	case	saturday	:
		cout	<< "saturday";
		break;
	case	sunday		:
		cout	<< "sunday";
		break;
	case	invalidDay	:
		cout	<< "invalid";
		break;
	default:
		cout 	<< "not recognized";
		break;
	}
}

string	daysOfTheWeek::toString()
{
	switch (theDay)	{
	case	monday	:
		return	"monday";
	case	tuesday	:
		return	"tuesday";
	case	wednesday	:
		return	"wednesday";
	case	thursday	:
		return	"thursday";
	case	friday		:
		return	"friday";
	case	saturday	:
		return	"saturday";
	case	sunday		:
		return	"sunday";
	case	invalidDay	:
		return	"invalid";
	default:
		return	"not recognized";
	}
}
daysOfTheWeek::daysOfTheWeek() {
	theDay	= invalidDay;
}

daysOfTheWeek::daysOfTheWeek(const daysOfTheWeek &other)
{
	theDay = other.theDay;
}


daysOfTheWeek::daysOfTheWeek(int i)
{
	theDay = i;
}

daysOfTheWeek::~daysOfTheWeek() {
	// TODO Auto-generated destructor stub
}

} /* namespace std */
